import { Component } from 'react';
import SaveItem from './SaveItem';
import Counter from './Counter'

class CreateItem extends Component{

    state = {
        items: [],
        draft: "",
    };

    addDraft = (draft) => {
        this.setState( { draft } )
    };
    
    addItem = (item) => {
        const { items } = this.state;
        this.setState( { items: [...items, item ], draft: "" } );
    };

    reset = () => {
        const { items } = this.state;
        this.setState( { items: [,], draft: "" } );
    };

    render(){
        const { items, draft } = this.state;

        return(
            <div>
                <SaveItem 
                onChange={this.props.onChange} 
                draft={draft} 
                addDraft={this.addDraft} 
                addItem={this.addItem}
                reset={this.reset}/> 

                {items.map((item, index) => <Counter key={index} objective={item[1]}><span>{item[0]} - {item[1]}</span></Counter>)}
            </div>

            )
    }
}

export default CreateItem;