import {useState} from 'react';

const Counter = (props) => {
    const [count, setCount] = useState(0);

    return(
        <div>
            <button onClick={() => {count > 0 && setCount(count - 1)}}>-</button>
            <button onClick={() => {count < props.objective && setCount(count + 1)}}>+</button>
            <span> {props.children} /</span>
            <span> {count} </span>
            { count >= props.objective && <span style={{color:"green"}}>DONE</span>}
        </div>
    );
}

export default Counter;