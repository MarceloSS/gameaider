import { Component } from 'react';

class SaveItems extends Component {
    state = {
        name: this.props.draft,
        objective:0,
    };

    setName = (event) => { 
        this.setState( { name: event.target.value } );
    }

    setObjective = (event) => {
        { event.target.value > 0 ? this.setState( { objective: event.target.value } ) : this.setState({objective: 0})}
    }

    componentWillUnmount() {
        
        this.props.addDraft( this.state.name );
    }
    
    handleAddItem = () => {
        const { name, objective } = this.state

        if(name && objective){
            this.props.addItem( [ this.state.name, this.state.objective ] );
            
            this.setState( { name: "" , objective: 0} );
        }
    };

    handleReset = () => {
        this.props.reset();
    }

    render () {
        const { name, objective } = this.state

        return(
            <span>
                <input onChange={this.setName} value={name}/>
                <input onChange={this.setObjective} value={objective} type="number" style={{width:"30px"}}/>
                <button onClick={this.handleAddItem}>Salvar</button>
                <button onClick={this.handleReset}>Reset</button>
            </span>
        )
    }
}

export default SaveItems;